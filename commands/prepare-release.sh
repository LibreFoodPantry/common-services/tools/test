#!/usr/bin/env bash

set -e

mkdir -p ./artifacts/release
echo "# Build ${1}" >> ./artifacts/release/test.yml
cat ./source/gitlab-ci/test.yml >> ./artifacts/release/test.yml
