#!/usr/bin/env bash

set -ue -o pipefail -o posix

if [ -z "$TEST_COMMAND" ] ; then
    echo "ERROR: TEST_COMMAND is not defined."
    exit 1
fi

if [ ! -e "$TEST_COMMAND" ] ; then
    echo "WARNING: $TEST_COMMAND does not exist. Skipping."
    exit 0
fi

if [ ! -x "$TEST_COMMAND" ] ; then
    echo "ERROR: $TEST_COMMAND is not executable."
    exit 1
fi

docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"

if ! docker pull "$PIPELINE_IMAGE_NAME" &> /dev/null ; then
    echo "PIPELINE_IMAGE_NAME=$PIPELINE_IMAGE_NAME"
    echo "Docker image not found. No image pulled."
else

    if [ -z "$IMAGE_NAME" ] ; then
        echo "ERROR: IMAGE_NAME is not defined."
        exit 1
    fi

    docker tag "$PIPELINE_IMAGE_NAME" "$IMAGE_NAME"
fi

echo "Running TEST_COMMAND=$TEST_COMMAND"
$TEST_COMMAND
